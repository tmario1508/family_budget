using System.Threading.Tasks;
using System.Collections.Generic;
using FamilyBudget.Entities;
using FamilyBudget.Api.Repository.Interfaces;
using MySqlConnector;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq;


namespace FamilyBudget.Api.Repository
{
    public class ProfileRepository : IProfileRepository
    {
        private const string _conStr =
            "server=localhost;user=root;pwd=Salitre13*;database=familybudget;port=3306;";

        private MySqlConnection _conn;

        public async Task<Profile> Add(Profile profile)
        {

            _conn = new MySqlConnection(_conStr);
            _conn.Open();

            profile.Deleted = 0;
            await _conn.InsertAsync<Profile>(profile);
            _conn.Close();

            return profile;
        }

        public async Task<bool> Delete(int id)
        {
            _conn = new MySqlConnection(_conStr);
            _conn.Open();

            var profile = await GetById(id);

            if(profile == null)
                return false;
            profile.Deleted = 1;

            await _conn.UpdateAsync<Profile>(profile); 

            // var result = await _conn.DeleteAsync<Profile>( new Profile{ Id = id});

            _conn.Close();

            return true;
        }

        public async Task<IEnumerable<Profile>> GetAll()
        {
            _conn = new MySqlConnection(_conStr);
            _conn.Open();

            //var profiles = await _conn.GetAllAsync<Profile>();

            // profiles = profiles.Where(p => p.Delete == false);

            var template = new Profile { Deleted = 0};
            var parameters = new DynamicParameters(template);
            var sql = "SELECT * FROM profile where Deleted = @Deleted";

            var profiles = await _conn.QueryAsync<Profile>(sql, parameters);

            _conn.Close();

            return profiles;
        } 

        public async Task<Profile> GetById(int id)
        {
            _conn = new MySqlConnection(_conStr);
            _conn.Open();

            var template = new Profile { Deleted = 0};
            var parameters = new DynamicParameters(template);
            var sql = "SELECT * FROM profile where Id = @Id && Deleted = @Deleted";

            var profiles = await _conn.QueryAsync<Profile>(sql, parameters);

            var profile = profiles.FirstOrDefault();

            //var profile = await _conn.GetAsync<Profile>(id);
            _conn.Close();

            return profile;
        }

        public async Task<Profile> Update(Profile profile)
        {
            _conn = new MySqlConnection(_conStr);
            _conn.Open();

            await _conn.UpdateAsync<Profile>(profile);

            _conn.Close();

            return profile;
        }
    }
}